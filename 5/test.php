<?php
/*
1. $color = ['Zöld', 'kék', 'lila']
Írj egy PHP szkriptet, amely a színeket a következő módon jeleníti meg:
kimenet:
a) Zöld, Kék, Lila,

b)
•Zöld
•Kék
•Lila
(2 pont)
 */
$color = ['Zöld', 'kék', 'lila'];
echo 'a) ';
foreach ($color as $v) {
    echo "<span style=\"text-transform: capitalize;\">$v</span>, ";
}
echo '<br>b) ';
echo '<ul>';
foreach ($color as $v) {
    echo "<li style=\"text-transform: capitalize;\">$v</li>";
}
echo '</ul>';

/*
Hozzon létre egy PHP szkriptet, amely megjeleníti a főváros és az ország nevét a alábbi $countries tömbből. Rendezd a listát az ország fővárosa szerint.

Kimenet :
The capital of Netherlands is Amsterdam
The capital of Greece is Athens
The capital of Germany is Berlin
 */
$countries = [
    "Italy" => "Rome",
    "Luxembourg" => "Luxembourg",
    "Belgium" => "Brussels",
    "Denmark" => "Copenhagen",
    "Finland" => "Helsinki",
    "France" => "Paris",
    "Slovakia" => "Bratislava",
    "Slovenia" => "Ljubljana",
    "Germany" => "Berlin",
    "Greece" => "Athens",
    "Ireland" => "Dublin",
    "Netherlands" => "Amsterdam",
    "Portugal" => "Lisbon",
    "Spain" => "Madrid",
    "Sweden" => "Stockholm",
    "United Kingdom" => "London",
    "Cyprus" => "Nicosia",
    "Lithuania" => "Vilnius",
    "Czech Republic" => "Prague",
    "Estonia" => "Tallin",
    "Hungary" => "Budapest",
    "Latvia" => "Riga",
    "Malta" => "Valetta",
    "Austria" => "Vienna",
    "Poland" => "Warsaw"
];
//rendezés
asort($countries, SORT_STRING);
foreach ($countries as $country => $capital) {
    echo "<br>The capital of $country is $capital";
}

/*
3.
Töröljön egy elemet a fenti PHP tömbből. Az elem törlése után az egész tömbkulcsokat normalizálni(rendezni) kell.
Kimenet :
array(5) { [0]=> int(1), [1]=> int(2), [2]=> int(3), [3]=> int(4), [4]=> int(5) }
array(4) { [0]=> int(1) [1]=> int(2) [2]=> int(3) [3]=> int(5) }
 */
echo '<pre>';
$x = [1, 2, 3, 4, 5];
var_dump($x);
//$drop = 4;//ezt fogjuk törölni, ha ezt használod array_searchel rá lehet keresni a tömbben
unset($x[3]);
sort($x);
var_dump($x);
echo '</pre>';

/*
4. $color = [4 => 'white', 6 => 'green', 11=> 'red'];

Kimenet : white
 */
$color = [4 => 'white', 6 => 'green', 11 => 'red'];
echo $color[4];

/*
5.
Írj egy PHP szkriptet, amely új elemet illeszt be egy tömbbe bármilyen pozícióra.
Kimenet :
eredeti tömb :
1 2 3 4 5
'$' beillesztése után (a kivánt pozició a 3-as elem utáni) :
1 2 3 $ 4 5
 */
echo '<pre>';
$x = [1, 2, 3, 4, 5];
var_dump($x);
$elem = '$';
//$pos = 3;
//eleje
$start = array_slice($x, 0, 3);
$end = array_slice($x, -2);
$x = $start;
$x[] = $elem;
//array_push($x, ...$end);//haladó megoldás
//$x= array_merge($x,$end);
foreach ($end as $v) {
    $x[] = $v;
}
var_dump($x);
echo '</pre>';

/*
6. Rendezd az alábbi tömböt a felsorolt kérelmek szerint a tömmbe név => életkor adatok vannak (4 pont)
["Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40"]
a) életkor szerint növekvő
b) Név szerint növekvő
c) életkor szerint csökkenő
d) Név szerint csökkenő
 */
$users = [
    "Sophia" => "31",
    "Jacob" => "41",
    "William" => "39",
    "Ramesh" => "40"
];
asort($users);
$sorted_users = $users;
echo '<pre>'.var_export($sorted_users,true).'</pre>';
ksort($users);
echo '<pre>'.var_export($users,true).'</pre>';
arsort($users);
echo '<pre>'.var_export($users,true).'</pre>';
krsort($users);
echo '<pre>'.var_export($users,true).'</pre>';

/*
7. Az alábbi mért hőmérsékletekből add meg az adott hónap átlaghőmérsékletét, az 5 legalacsonyabb és az 5 legmagasabb különböző értéket
Mért értékek : 28, 19, 20, 20, 29, 26, 25, 33, 30, 30, 25, 26, 22, 22, 28, 29, 27, 28, 27, 31, 28, 30, 22, 24, 23, 24, 25, 27, 25, 24
Kimenetek :
Hőátlag : 25.9
5 legalacsonyabb: 19, 20, 22, 23, 24
5 legmagasabb : 28, 29, 30, 31, 33
 */
$temps = [28, 19, 20, 20, 29, 26, 25, 33, 30, 30, 25, 26, 22, 22, 28, 29, 27, 28, 27, 31, 28, 30, 22, 24, 23, 24, 25, 27, 25, 24];
$average_temp = array_sum($temps)/count($temps);
echo "Hőátlag: $average_temp";

$temps_2 = array_unique($temps);//csak egyedi értékekkel megyünk tovább
sort($temps_2);
$min_five = array_slice($temps_2,0,5);
$max_five = array_slice($temps_2,-5);
//var_dump($min_five);
echo '<br>5 Legalacsonyabb'.implode(',',$min_five);
echo '<br>5 Legmagasabb'.implode(',',$max_five);

/*
8. Írjon egy PHP szkriptet a következő tömbkészlet maximális és minimális pontjainak megtalálásához.
Minta tömbök:
$marks1 = [360,310,310,330,313,375,456,111,256];
$marks2 = [350,340,356,330,321];
$marks3 = [630,340,570,635,434,255,298];
kimenet:
Maximális pontszám: 635
Minimális pontszám: 111
 */
$marks1 = [360,310,310,330,313,375,456,111,256];
$marks2 = [350,340,356,330,321];
$marks3 = [630,340,570,635,434,255,298];
$max1 = max($marks1);
$max2 = max($marks2);
$max3 = max($marks3);
$max = max($max1,$max2,$max3);
echo "<br>max: $max";

//egy lépésben
//$max = max(...$marks1,...$marks2,...$marks3);
$max = max(array_merge($marks1,$marks2,$marks3));
//var_dump($max);
echo "<br>max: $max";
$min = min(array_merge($marks1,$marks2,$marks3));
//var_dump($min);
echo "<br>min: $min";
<?php
//űrlap adatok feldolgozása, ha vannak
if (!empty($_POST)) {
    $errors = [];//üres hiba tömb
    //...mezők hibakezelése
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //név mező min 3 karakter
    $name = strip_tags(trim(filter_input(INPUT_POST, 'name')));//szűrés és védelem
    if (mb_strlen($name, "utf-8") < 3) {
        $errors['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }

    //email formátum ellenőrzése
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);null ha nincs meg a mező, false ha nem email, maga a string ha jó emailnek látszik
    if (!$email) {
        $errors['email'] = '<span class="error">Hibás formátum!</span>';
    }

    //jelszavak hibakezelése
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    //hossz ellenőrzés
    if(mb_strlen($password,'utf-8') < 6 ){
        $errors['password'] = '<span class="error">Legalább 6 karakter!</span>';
    }elseif($password !== $repassword){//egyezés vizsgálata
        $errors['repassword'] = '<span class="error">A beírt jelszavak nem egyeztek!</span>';
    }else{
        //$secret_key = "S3cR3t_K3y!";
        //formailag jó a jelszó
        //var_dump(md5($password.$secret_key));//nem igazán biztonságos még így se...

        //mindenképp nem visszafordítható algoritmussal kell kódolni
        $password = password_hash($password,PASSWORD_BCRYPT);
        var_dump($password);
    }



    //életkort ha adtak meg akkor legyen egész szám 1-120 között
    if (filter_input(INPUT_POST, 'age') != '') {//kell e kezelni a mezőt
        $options = [
            'options' => [
                'min_range' => 1,
                'max_range' => 120
            ]
        ];
        $age = filter_input(INPUT_POST, 'age', FILTER_VALIDATE_INT, $options);
        if (!$age) {
            $errors['age'] = '<span class="error">Hibás formátum!</span>';
        }
        //var_dump($age);
    }
    //adatvédelem checkbox kötelezően kipipálandó
    if (filter_input(INPUT_POST, 'terms') === null) {
        $errors['terms'] = '<span class="error">Kötelező kipipálni!</span>';
    }

    echo '<pre>' . var_export($errors, true) . '</pre>';
    if (empty($errors)) {
        //nem volt hiba
        //echo "<div>Hello kedves <b>$name</b></div>";
        //adatok 'rendberakása'
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'age' => $age ?? '' //mivel ez nem kötelező adat, nem biztos hogy létezik, olyan esetben legyen üres string
        ];
        //operátor: $a = isset($var) ? $var : '' -> $a = $var ?? '';
        //időpont
        $now = date('Y-m-d H:i:s');//https://www.php.net/manual/en/datetime.format.php
        $data['time_created'] = $now;
        echo '<pre>' . var_export($data, true) . '</pre>';

        //mit csináljunk az adatokkal? ¯\_(ツ)_/¯
        //tároljuk el fileban

        //json
        $dataJSON = json_encode($data);
        //echo $dataJSON;

        //file és mappa beállítások ellenőrzés - létrehozás
        $dir = 'files/';//ebbe a mappába szeretnénk dolgozni
        //létezik e?
        if(!is_dir($dir)){
            //nem létezik, hozzuk létre
            mkdir($dir, 0755, true);
        }
        //filenév és kiterjesztés
        $fileName = 'user.json';

        //adatok tárolása fileban (felülírás mindig)
        file_put_contents($dir.$fileName, $dataJSON);
        /*
         * @todo HF: átnézni php.netről (filesystem)
         fopen(),fread(),fwrite(),fclose()
         */
        exit();
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlapkezelés</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            width: 100%;
            max-width: 480px;
            margin: 0 auto;
            display: flex;
            flex-direction: column;
        }

        label {
            display: flex;
            flex-direction: column;
            margin: 5px 0;
        }

        input:not(type="checkbox") {
            display: block;
        }

        .error {
            color: red;
            font-size: .8em;
            font-style: italic;
        }

    </style>
</head>
<body>
<h1>Űrlapelemek feldolgozása azonos fileban és mező-minták</h1>
<section class="myForm">
    <?php
    //Pure PHP űrlap
    $form = '<form method="post">';//űrlap nyitás

    //név mező
    $form .= '<label>
            <span>Név (minimum 3 karakter)</span>
            <input type="text" name="name" value="' . filter_input(INPUT_POST, 'name') . '"
                   placeholder="Gipsz Jakab">';
    //hiba ha van, hozzáfűzzük az input elemhez a label zárás előtt
    $form .= getError('name');
    $form .= '</label>';

    //email
    $form .= '<label>
            <span>Email (kötelező)</span>
            <input type="text" name="email" value="' . filter_input(INPUT_POST, 'email') . '"
                   placeholder="email@cim.hu">';
    $form .= getError('email');
    $form .= '</label>';

    //jelszó 1 és 2
    $form .= '<label>
            <span>Jelszó (kötelező)</span>
            <input type="password" name="password" value="" placeholder="******">';
    $form .= getError('password');
    $form .= '</label>';

    $form .= '<label>
            <span>Jelszó újra (kötelező)</span>
            <input type="password" name="repassword" value="" placeholder="******">';
    $form .= getError('repassword');
    $form .= '</label>';

    //kor
    $form .= '<label>
        <span>Életkor [1-120] (nem kötelező de, ha megadják egész szám)</span>
        <input type="text" name="age" value="' . filter_input(INPUT_POST, 'age') . '" placeholder="18">';
    $form .= getError('age');
    $form .= '</label>';


    //terms
    $checked = filter_input(INPUT_POST, 'terms') ? 'checked' : '';

    $form .= '<label>
           <span>
                <input type="checkbox" name="terms" value="1" '.$checked.'> Elolvastam és megértettem az <a
                       href="#" target="_blank">adatkezelési irányelvek</a>et!</span>';
    $form .= getError('terms');
    $form .= '</label>';


    //gomb és form zárás
    $form .= '<button>mehet</button>
    </form>';
    //kiírás egy lépésben
    echo $form;


    ?>
</section>
</body>
</html>
<?php
/**
 * Saját hibaüzenet kiíró eljárás az $errors tömbből, mezőnév alapján
 * @param $fieldName string
 * @return mixed|string
 */
function getError($fieldName)
{
    //hibatömb, ha van akkor érjük el (az eljárás idejére legyen globális)
    global $errors;
    //ha van benne az adott elem
    if (isset($errors[$fieldName])) {
        return $errors[$fieldName];//térjünk vissza vele (hibaüzenet)
    }
    return '';
}
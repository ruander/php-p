<?php
//Adatbázis szerver elérése és a kapcsolat felépítése ($link)
$dbHost = 'localhost';
$dbUser = 'root';
$dbPassword = '';
$dbName = 'classicmodels';
$link = @mysqli_connect($dbHost, $dbUser, $dbPassword, $dbName) or die("db GEBASZ: " . mysqli_connect_error());
//a $link resource (mysqli) tipusu erőforrás reprezentálja az adatbázis egészét

//Pl összes alkalmazott lekérése, névvel
$qry = "SELECT CONCAT(firstname,' ',lastname) name FROM employees";//lekérés összeállítása

$result = mysqli_query($link,$qry) or die(mysqli_error($link));//lekérés futtatása és tárolása változóban vagy ÁLLJ! resource tipusu (mysqli)

//eredmények 'kibontása' - fetch
/*$row = mysqli_fetch_row($result);
echo "<pre>" . var_export($row, true) . "</pre>";

$row = mysqli_fetch_assoc($result);
echo "<pre>" . var_export($row, true) . "</pre>";

$row = mysqli_fetch_array($result);
echo "<pre>" . var_export($row, true) . "</pre>";

$row = mysqli_fetch_object($result);
echo "<pre>" . var_export($row, true) . "</pre>";

$rows = mysqli_fetch_all($result,MYSQLI_ASSOC);//összes kibontása
echo "<pre>" . var_export($rows, true) . "</pre>";

$row = mysqli_fetch_row($result);
echo "<pre>" . var_export($row, true) . "</pre>";*/

echo '<ul>';
//alkalmazottak listaelemei ciklusban, amig kapunk eredményt, addig feldolgozzuk
while(null !== $row = mysqli_fetch_assoc($result)){
    echo "<li>{$row['name']}</li>";
}

echo '</ul>';


echo "<h2>5. irodánként mennyit (összeg) rendeltek?</h2>";
$qry = "SELECT 
            off.country,
            off.city,
            SUM(quantityordered*priceeach) usd
        FROM offices off
        LEFT JOIN employees e 
        ON off.officecode = e.officecode
        LEFT JOIN customers c 
        ON e.employeenumber = salesrepemployeenumber
        LEFT JOIN orders o
        ON c.customernumber = o.customernumber
        LEFT JOIN orderdetails od
        ON o.ordernumber = od.ordernumber
        GROUP BY off.officecode
        ORDER BY usd DESC";//lekérés sql stringje
$result = mysqli_query($link,$qry) or die(mysqli_error($link));//lekérés
echo '<ul>';
//alkalmazottak listaelemei ciklusban, amig kapunk eredményt, addig feldolgozzuk
while(null !== $row = mysqli_fetch_assoc($result)){
    echo "<li>{$row['country']} - {$row['city']} : <b>{$row['usd']}</b></li>";
    //echo '<li>'.$row['country'].' - '.$row['city'].' : <b>'.$row['usd'].'</b></li>';
}

echo '</ul>';

echo "<h2>15. listázzuk ki az amerikai (az iroda van ott) megrendelőket: név, város, ország</h2>";
$qry = "SELECT 
            c.country,
            c.city,
            customername
        FROM customers c
        LEFT JOIN employees e
        ON e.employeenumber = salesrepemployeenumber    
        LEFT JOIN offices off
        ON off.officecode = e.officecode
        WHERE off.country = 'USA'";//lekérés sql stringje
$result = mysqli_query($link,$qry) or die(mysqli_error($link));//lekérés
echo '<ul>';
//alkalmazottak listaelemei ciklusban, amig kapunk eredményt, addig feldolgozzuk
while(null !== $row = mysqli_fetch_assoc($result)){
    echo "<li><b>{$row['customername']}</b> : {$row['country']} - {$row['city']}</li>";
}

echo '</ul>';
<!doctype html>
<html lang="hu">
<head>
 <title>első php fileom</title>
 <meta charset="utf-8">
</head>
<body>
Helo World!
<?php 
//php egy soros megjegyzés
/*
php 
blokk komment
*/
print 'Helo World!';//kiírás a standard outputra utasítás (print), operátorok: '',"" string határolók, operátor: ; -> utasítás lezárása
//változók - primitívek (lokális)
//$variableName = 'value'; ->deklarálás, operátor: $ -> változó; operátor: = -> értékadó operátor
//a változó neve: case sensitive (a nem egyenlő A), nem kezdődhet számmal, nem tartalmazhat speciális karaktert(kivéve: _)
//cc: legyen beszédes
$user_name = 'Horváth György';// xy_zz -> snake_case | tipus: string
$userAge = 46;//wordAnotherWord ... camelCase | tipus: integer (int)
$is_educator = true;//tipus: logikai - boolean (bool)
$pi = 3.14;//tipus: lebegő pontos Floating point number (float)

//információ kiírása std outputra fejlesztés alatt egy vagy több változóról
var_dump($user_name,$userAge,$is_educator,$pi);

//állandók (globálisak)

const g = 9.81;
const PI = 3.14;
const c = 300000; //km/s
echo '<pre>';
var_dump(c);
echo '</pre>';
//
$user_name = "nameless one";
echo $user_name;
//c = 300005;//parse error -> állandó értéke nem változhat


//nem primitív értékek
//tömb (array) array(1,2,3,4) => [1,2,3,4]
$colors = ['red','green','blue'];
echo '<pre>';
var_dump($colors);
echo '</pre>';

//tömb bővítése automatikus indexre 1 elemmel
$colors[] = 'grey';
echo '<pre>';
var_dump($colors);
echo '</pre>';

//tömb bővítése irányított indexre 1 elemmel
$colors[100] = 'grey';
echo '<pre>';
var_dump($colors);
echo '</pre>';

//asszociatív tömb
/*nem az igazi
$user = [
	1 => 'teszt...',
	'user_name' => 'Horváth György',
	'user_email' => 'hgy@iworkshop.hu',
	'user_id' => 5
];
*/
$user = [
	'name' => 'Horváth György',
	'email' => 'hgy@iworkshop.hu',
	'id' => 5
];
echo '<pre>';
var_dump($user);
echo '</pre>';

//tömb 1 értékének elérése
//echo $user;//Array és notice jelzés
echo '<i>';
echo $user['name'];
echo '</i>';
echo '|';
echo $user['email'];
//^^^^^helyettˇˇˇˇˇˇ
echo '<br><i>'.$user['name'].'</i> | '.$user['email'];//operátor: . -> konkatenáció (összefűzés) (stringé konvertálja mind2 elemet)


//műveletek primitívekkel:
$a=5;
$b=4/7;
$c='25';
$d=2500;
$e=false;//true - 1 | false - 0
$f='e5. oldal: teszt...';
//+,-,/,*,négyzetgyök,hatvány

$result_1 = $a + $b + $c;//30,5...
$result_2 = $a + $c - $d;//-2470
$result_3 = $result_1 * $result_2;//~75000
$result_4 = $d / $c;//100
$result_5 = $e * $d;//2500
$result_6 = $f + $a;//? warning non numeric value: string + number értelmezési hiba 
$result_7 = $a . $c;//'525'
echo '<pre>';
var_dump($result_1,$result_2,$result_3,$result_4,$result_5,$result_6,$result_7);
echo '</pre>';

//hatványozás x^y -> pow(x,y)
//$a négyzet
//$result = $a * $a; :) 
$result_8 = pow($a,2);//25
//négyzetgyök
$result_9 = sqrt($d);//50
echo '<pre>';
var_dump($result_8,$result_9);
echo '</pre>';

//elágazás
/*
if(feltétel){
	//igaz ág
}else{
	//hamis ág
}
*/
if($a < 10){
	echo '<br>Az '.$a.' értéke kisebb, mint 10';
}else{
	echo '<br>Az $a értéke 10 vagy nagyobb';
}

if($a > 5){
	echo "<br>Az $a \"értéke nagyobb\" mint 5.";
}else{
	echo "<br>Az \\\$a értéke 5, vagy kevesebb.";// operátor \ -> escape, az utána következő karaktert kilépteti a nyelvi elemek végrehajtásából
}

//1. Írjon egy php programot, amely kiszámolja és kiírja a 2 m élhosszúságú kocka felületét.
$a = 2;
$surface = $a*$a*6;
echo "<div>Egy $a m élhosszúságú kocka felülete $surface m<sup>2</sup></div>";

?>
</body>
</html>
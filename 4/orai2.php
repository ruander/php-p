<?php
//űrlap adatok feldolgozása, ha vannak
if (!empty($_POST)) {
    $errors = [];//üres hiba tömb
    //...mezők hibakezelése
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //név mező min 3 karakter
    $name = filter_input(INPUT_POST,'name');
    //szövegvégi (és eleje) spacek eltávolítása (és egyéb felesleges karakterek)
    $name = trim($name);//létezik ltrim, rtrim
    //html tagek eltávolítása (védelem scriptek ellen 1):
    $name = strip_tags($name);
    //var_dump(mb_strlen($name,"utf-8"));//string hossza
    if( mb_strlen($name,"utf-8") < 3 ){
        $errors['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }

    //email formátum ellenőrzése
    $email = filter_input(INPUT_POST,'email');
    var_dump($email);
    //email ellenőrzése saját módszerrel (órai feladat)
    /** @todo: HF:
     * legalább 1 @ karaktert kell tartalmaznia
     * @ előtt legalább 1 karakter ami nem lehet space
     * @után legalább 1 .
     * . előtt és után legalább 1 karakter
     */
    $test = explode('@',$email);//bontás kukacnál
    echo '<pre>' . var_export($test, true) . '</pre>';
    $test2 = explode('.',$test[1]);//bontás pontnál
    echo '<pre>' . var_export($test2, true) . '</pre>';



    if(true){
        $errors['email'] = '<span class="error">Hibás formátum!</span>';
    }


    echo '<pre>' . var_export($errors, true) . '</pre>';
    if(empty($errors)){
        //nem volt hiba
        echo "<div>Hello kedves <b>$name</b></div>";
       // exit('nem volt hiba');
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlapkezelés</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            width: 100%;
            max-width: 480px;
            margin: 0 auto;
            display: flex;
            flex-direction: column;
        }

        label {
            display: flex;
            flex-direction: column;
            margin: 5px 0;
        }

        input:not(type="checkbox") {
            display: block;
        }

        .error {
            color: red;
            font-size: .8em;
            font-style: italic;
        }

    </style>
</head>
<body>
<h1>Űrlapelemek feldolgozása azonos fileban és mező-minták</h1>
<section class="myForm">
    <form method="post">
        <label>
            <span>Név (minimum 3 karakter)</span>
            <input type="text" name="name" value="<?php echo filter_input(INPUT_POST,'name'); ?>" placeholder="Gipsz Jakab">
            <!--<span class="error">hibaüzenet ha van</span>-->
            <?php
            //ha létezik a hibaelemünk akkor itt írjuk ki
            if(isset($errors['name'])){
                echo $errors['name'];
            }

            ?>
        </label>
        <label>
            <span>Email (kötelező)</span>
            <input type="text" name="email" value="<?php echo filter_input(INPUT_POST,'email'); ?>" placeholder="email@cim.hu">
            <?php
            //ha létezik a hibaelemünk akkor itt írjuk ki
            if(isset($errors['email'])){
                echo $errors['email'];
            }

            ?>
        </label>
        <label>
            <span>Életkor (nem kötelező de, ha megadják egész szám)</span>
            <input type="text" name="age" value="" placeholder="18">
            <span class="error">hibaüzenet ha van</span>
        </label>
        <label>
            <span>
                <input type="checkbox" name="terms" value="1"> Elolvastam és megértettem az <a href="#" target="_blank">adatkezelési
                    irányelvek</a>et!
            </span>
            <span class="error">hibaüzenet ha van</span>
        </label>

        <button>mehet</button>
    </form>
</section>
<!--@todo HF 2: Jelszómezőkkel ki kell egészíteni az űrlapot
 jelszó (password) és jelszó újra (repassword)
 kötelező mező
 legalább 6 karakter hosszú legyen
 a két mezőbe írt jelszónak egyeznie kell
 hibaüzenetek: password: legalább 6 karakter!
 repassword: a beírt jelszavak nem egyeztek!
 A jelszó mezőbe SOHA nem írsz vissza valuet
 -->
</body>
</html>

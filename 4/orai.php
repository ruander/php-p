<?php
//űrlap adatok feldolgozása, ha vannak
if (!empty($_POST)) {
    $errors = [];//üres hiba tömb
    //...mezők hibakezelése
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //név mező min 3 karakter
    $name = filter_input(INPUT_POST, 'name');
    //szövegvégi (és eleje) spacek eltávolítása (és egyéb felesleges karakterek)
    $name = trim($name);//létezik ltrim, rtrim
    //html tagek eltávolítása (védelem scriptek ellen 1):
    $name = strip_tags($name);
    //var_dump(mb_strlen($name,"utf-8"));//string hossza
    if (mb_strlen($name, "utf-8") < 3) {
        $errors['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }

    //email formátum ellenőrzése
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);null ha nincs meg a mező, false ha nem email, maga a string ha jó emailnek látszik
    if (!$email) {
        $errors['email'] = '<span class="error">Hibás formátum!</span>';
    }

    //életkort ha adtak meg akkor legyen egész szám 1-120 között
    if (filter_input(INPUT_POST, 'age') != '') {//kell e kezelni a mezőt
        $options = [
            'options' => [
                'min_range' => 1,
                'max_range' => 120
            ]
        ];
        $age = filter_input(INPUT_POST, 'age', FILTER_VALIDATE_INT, $options);
        if (!$age) {
            $errors['age'] = '<span class="error">Hibás formátum!</span>';
        }
        var_dump($age);
    }
    //adatvédelem checkbox kötelezően kipipálandó
    if (filter_input(INPUT_POST, 'terms') === null) {
        $errors['terms'] = '<span class="error">Kötelező kipipálni!</span>';
    }

    echo '<pre>' . var_export($errors, true) . '</pre>';
    if (empty($errors)) {
        //nem volt hiba
        //echo "<div>Hello kedves <b>$name</b></div>";
        //adatok 'rendberakása'
        $data = [
            'name' => $name,
            'email' => $email,
            'age' => $age ?? '' //mivel ez nem kötelező adat, nem biztos hogy létezik, olyan esetben legyen üres string
        ];
        //operátor: $a = isset($var) ? $var : '' -> $a = $var ?? '';
        //időpont
        $now = date('Y-m-d H:i:s');//https://www.php.net/manual/en/datetime.format.php
        $data['time_created'] = $now;
        echo '<pre>' . var_export($data, true) . '</pre>';

        //mit csináljunk az adatokkal? ¯\_(ツ)_/¯
        //tároljuk el fileban
        //fileban primitivet tudunk tárolni (string)
        //tömb-string konverziók
        //sorozat
        $dataSerialized = serialize($data);
        echo $dataSerialized;

        //visszalakítható
        $dataFromSerialized = unserialize($dataSerialized);
        //egyezőség teszt
        if($dataFromSerialized === $data){
            echo '<div>a két tömb egyezik!</div>';
        }else{
            echo '<div>a két tömb NEM egyezik!</div>';

        }
        echo '<pre>' . var_export($dataFromSerialized, true) . '</pre>';

        //json
        $dataJSON = json_encode($data);
        echo $dataJSON;

        //visszalakítható
        $dataFromJSON = json_decode($dataJSON,true);//true -> asszociativ tömböt kapjunk, enélkül object lesz

        //egyezőség teszt
        if($dataFromJSON === $data){
            echo '<div>a két tömb egyezik!</div>';
        }else{
            echo '<div>a két tömb NEM egyezik!</div>';
        }


        echo '<pre>' . var_export($dataFromJSON, true) . '</pre>';
        exit();
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlapkezelés</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            width: 100%;
            max-width: 480px;
            margin: 0 auto;
            display: flex;
            flex-direction: column;
        }

        label {
            display: flex;
            flex-direction: column;
            margin: 5px 0;
        }

        input:not(type="checkbox") {
            display: block;
        }

        .error {
            color: red;
            font-size: .8em;
            font-style: italic;
        }

    </style>
</head>
<body>
<h1>Űrlapelemek feldolgozása azonos fileban és mező-minták</h1>
<section class="myForm">
    <form method="post">
        <label>
            <span>Név (minimum 3 karakter)</span>
            <input type="text" name="name" value="<?php echo filter_input(INPUT_POST, 'name'); ?>"
                   placeholder="Gipsz Jakab">
            <!--<span class="error">hibaüzenet ha van</span>-->
            <?php
            //ha létezik a hibaelemünk akkor itt írjuk ki
            if (isset($errors['name'])) {
                echo $errors['name'];
            }

            ?>
        </label>
        <label>
            <span>Email (kötelező)</span>
            <input type="text" name="email" value="<?php echo filter_input(INPUT_POST, 'email'); ?>"
                   placeholder="email@cim.hu">
            <?php
            //ha létezik a hibaelemünk akkor itt írjuk ki
            if (isset($errors['email'])) {
                echo $errors['email'];
            }

            ?>
        </label>
        <label>
            <span>Életkor [1-120] (nem kötelező de, ha megadják egész szám)</span>
            <input type="text" name="age" value="<?php echo filter_input(INPUT_POST, 'age'); ?>" placeholder="18">
            <?php
            //ha létezik a hibaelemünk akkor itt írjuk ki
            if (isset($errors['age'])) {
                echo $errors['age'];
            }

            ?>
        </label>
        <label>
            <?php
            //ha ki volt pipálva, maradjon úgy
            /*
            $checked = '';
            if(filter_input(INPUT_POST,'terms')){
                $checked = 'checked';
            }
            */
            $checked = filter_input(INPUT_POST, 'terms') ? 'checked' : '';
            ?>
            <span>
                <input type="checkbox" name="terms" value="1" <?php echo $checked; ?>> Elolvastam és megértettem az <a
                        href="#" target="_blank">adatkezelési
                    irányelvek</a>et!
            </span>
            <?php
            echo getError('terms');
            ?>
        </label>

        <button>mehet</button>
    </form>
</section>
</body>
</html>
<?php
/**
 * Saját hibaüzenet kiíró eljárás az $errors tömbből, mezőnév alapján
 * @param $fieldName string
 * @return mixed|string
 */
function getError($fieldName)
{
    //hibatömb, ha van akkor érjük el (az eljárás idejére legyen globális)
    global $errors;
    //ha van benne az adott elem
    if (isset($errors[$fieldName])) {
        return $errors[$fieldName];//térjünk vissza vele (hibaüzenet)
    }
    return '';
}
<?php
//1. Készítsen egy olyan ciklust, amely egymás után menü feliratokat rak ki.
echo '<nav><ul>';//nav és ul nyitás
//menüpontok
for ($i = 1; $i < 6; $i++) {
    echo '<li><a href="#">menü ' . $i . '</a></li>';//menüpontok
}
echo '</ul></nav>';//nav és ul zárás
//menüpontok segédtömbbel, egy lépésben kiírva
$myMenu = [
    0 => 'home',
    1 => 'about',
    2 => 'services',
    3 => 'contact',
    4 => 'extra menu'
];

$menu = '<nav><ul>';//nyitótagek

foreach($myMenu as $menuID => $menuItem){
    //menüpontok
    $menu .= '<li><a href="?page=' . $menuID . '">' . $menuItem . '</a></li>';//menüpontok hozzáfűzése
}

$menu .= '</ul></nav>';//zárások hozzáfűzése
//kiírás a bodyban
//echo $menu;


//2 és 3. feladat: számolása
//2
    $sum=0;
    for($i=1;$i<=100;$i++){
        $sum += $i;
    }
    $feladat_2 = "<br>A számok összege 1-100-ig: $sum";

//3
$m=1;
for($i=1;$i<=7;$i++){
    $m *= $i;
}
$feladat_3 = "<br>A számok szorzata 1-7-ig: $m";
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
echo $menu;

//2. és 3. feladat eredményeinek kiírása
echo $feladat_2;
echo $feladat_3;

/**
 * @todo HF: alapok feladatgyűjtemény text fileból: Mindent amit tudsz, +feladatgyüjtemény pdf ből 4,5 feladatok
 */
?>
</body>
</html>

<?php
//14.Írjon egy programot, amely kiszámolja az 1-től 10-ig terjedő egész számok átlagát ciklus utasítás használatával.
$sum=0;
for($i=1;$i<=10;$i++){
    $sum += $i;
}//ciklus vége
//$i itt 11
echo "<br>Az 1-től 10-ig terjedő egész számok átlaga: ". $sum/10;//vagy  $sum/($i-1)


//15.Írjon egy programot, amely kiszámolja az 1-től 20-ig terjedő egész számok négyzetét, de csak azokat az értékeket írja ki, amely nagyobb, mint 30. Használjon ciklus utasítást.
for($i=1;$i<=20;$i++){
    $x= pow($i,2);
    if($x > 30){
        echo "<br>$x";
    }
}

//16.Írjon egy programot, amely kiszámolja az 1*3*5*7*9*11*13*15*17*19 értéket ciklus utasítás segítségével.
$mltp=1;
for($i=1;$i<20;$i+=2){
    $mltp*=$i;
}
echo "<br>1*3*5*7*9*11*13*15*17*19 = $mltp";

//16.b)Írjon egy programot, amely kiszámolja az 1*3*5*7*9*11*13*15*17*19 értéket ciklus utasítás segítségével.
$mltp=1;
$max = 30;
$text = '';//ide gyűjtjük a kiírandó elemeket
for($i=1;$i<$max;$i+=2){
    $mltp *= $i;
    //szorzat felirat fűzése
    $text .= $i."*";//opertátor: $text.="valami" -> $text = $text . "valami"
}
//a text végén marad egy "*" a fűzés miatt, ez eltávolítható trim()-el
$text = rtrim($text, '*');//jobbról, van ltrim(), trim()
echo "<br>$text = $mltp";
//16.c)Írjon egy programot, amely kiszámolja az 1*3*5*7*9*11*13*15*17*19 értéket ciklus utasítás segítségével.
$mltp=1;
$numbers=[];//itt lesznek a szorzat értékei
for($i=1;$i<$max;$i+=2){
    $mltp*=$i;
    //gyűjtsük egy tömbbe az értékeket
    $numbers[]=$i;
}
$text = implode("*",$numbers);//php.net
echo '<pre>'.var_export($numbers,true).'</pre>';
echo '<pre>'.var_export($text,true).'</pre>';
//tömb értékeinek összefűzése stringgé, bármilyen 'elválasztóval'
echo "<br>$text = $mltp";

//17.Írjon egy programot, amelyben egy 20 elemű tetszőleges tömböt hoz létre és kiírja a tömb minden második elemét. (a végén var_export a tömbre)
$qty = 20;
$numbers = [];
$i =1;
while( count($numbers) < $qty ){
    $number = rand(1,1000);
    $numbers[]=$number;
    //ha i páros, kiírjuk a számot
    if($i%2 == 0){
        echo "<br>$number";
    }
    $i++;
}
echo '<pre>'.var_export($numbers,true).'</pre>';

$numbers = [];
for($i=1;$i<=$qty;$i++){
    $number = rand(1,1000);
    $numbers[$i]= $number;
    //ha i páros, kiírjuk a számot
    if($i%2 == 0){
        echo "<br>$number";
    }

}
echo '<pre>'.var_export($numbers,true).'</pre>';
/////////////////////////////
$numbers = [];//üres tömb
while(count($numbers)<20){
    $numbers[]=rand(1,1000);
}
echo '<pre>'.var_export($numbers,true).'</pre>';
//minden elem kiírása: (foreach)
foreach($numbers as $value){
    echo "<br>$value";
}

//minden 2. elem kiírása: (foreach)
$i=1;
foreach($numbers as $value){
    //ha i páros, kiírjuk a számot
    if($i%2 == 0){
        echo "<br>$value";
    }
   $i++;
}
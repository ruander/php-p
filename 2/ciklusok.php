<?php
//bitbucket: https://bitbucket.org/ruander/php-p/src/master/
/*
A for ciklus

for(ciklusváltozó kezdeti értéke; belépési feltétel vizsgálata; ciklusváltozó léptetése){
    ciklusmag
}
*/
//írjuk ki a számokat 1-10ig
for ($i = 1; $i <= 10; $i = $i + 1) {
    echo "<br>$i";
}

for ($i = 0; $i < 10; $i++) {//operátor: $i = $i + 1 ->$i++, $i--
    echo "<br>". ($i+1);
}

//adjuk össze a számokat 1-10ig és írjuk ki az eredményt
$sum = 0;
for ($i = 1; $i <= 10; $i++) {
    //növeljük az összeget a ciklusváltozó értékével
    $sum += $i; //$sum = $sum + $i; $a = $a*7 -> $a *=7; -=, /=
}
//kiírás
echo "<br> A számok összege $sum";//55


/**
A while ciklus
while(feltétel){//amíg a feltétel igaz(true)
    ciklusmag
 }
 *
 hátultesztelő változata
 do{
    ciklusmag
 }while(feltétel)
 *
 */
//az előző for-os példa while-al
$i=1;
while($i<=10){
    echo "<br>$i";
    $i++;//léptetés
}

//generáljunk véletlen számokat 1-10ig amíg nem kapunk 8 vagy nagyobbat
//$randomNumber = rand(1,10);
echo '<h2>A generált számok:</h2>';
//var_dump($randomNumber);
$n=1;
while( ($randomNumber = rand(1,10)) < 8){
    var_dump($randomNumber);
    $n++;
}
echo "Az első 8 vagy nagyobb szám a $n. esetben a $randomNumber volt: ";

//töltsünk fel egy 10 elemű tömböt véletlen számokkal (1-1000)
$numbers = [];//üres tömb
/*for($i=1; $i<=10; $i++){
    $numbers[]=rand(1,1000);
}*/
while( count($numbers) < 10 ){//amíg 10nél kevesebb elem van, tegyünk bele egy újabbat
    $numbers[]=rand(1,1000);
}

echo '<pre>'.var_export($numbers,true).'</pre>';

/*
$numbers = [];//üres tömb
//működik, de NE!
for(; count($numbers) < 10 ;){
    $numbers[]=rand(1,1000);
}
echo '<pre>'.var_export($numbers,true).'</pre>';
*/
/*
 Ha egy szám páros akkor 2vel osztva a maradék 0
if( $i%2 == 0){
    // i páros
}else{
    //i páratlan
}
 */

/*
 A foreach ciklus
foreach($tomb as [$key =>] $value){
    ciklusmag
}
 */
$test = [
    'alma',
    25*13,
    'hgy@email.email',
    true,
    65/17,
    'token' => 'dfshju312vu31isdvg12dfvs',
    //'user_id' => 35
];
$test['user_id'] = 35;
echo '<pre>'.var_export($test,true).'</pre>';
//tömb bejárása és kulcs-érték párok kiírása (1 dimenzió esetén)

foreach($test as $key => $value){
    //$key és $value elérhető ;$value lehet nem primitiv is
    //var_dump($key,$value);
    echo "<br>Az aktuális kulcs: $key, value: $value";
}
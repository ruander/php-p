<?php


//adatfeldolgozás, ha van mit, azaz nem üres a POST
if (!empty($_POST)) {
    //hibaüzenet kialakítása
//szuperglobális post elemei:
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    $errors = [];//ide gyűjtjük a hibákat ugyanazokon a kulcsokon ahogyan a POSTban kapjuk
    //név nem lehet üres
    $name = filter_input(INPUT_POST,'name');
    if($name == ''){
        $errors['name'] = 'Kötelező kitölteni!';
    }

    echo '<pre>' . var_export($errors, true) . '</pre>';
    //hibekezelés vége
    if(empty($errors)){
        //minden ok
    }
}


?><!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozás azonos file-ban</title>
</head>
<body>

<h2>a POST metódus</h2>
<form method="post">
    <label>
        <span>Név<sup>*</sup></span>
        <input type="text" name="name" placeholder="Gipsz Jakab" value="">
        <?php
        //hiba kiírása, ha létezik
        if(isset($errors['name'])){
            echo $errors['name'];
        }
        ?>
    </label>
    <label>
        <span>Email<sup>*</sup></span>
        <input type="text" name="email" placeholder="email@cim.hu" value="">
    </label>
    <button>Mehet</button>
</form>
</body>
</html>
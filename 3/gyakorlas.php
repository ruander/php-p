<?php
/*
19.Írjon egy programot ciklus utasítást használva, amely az alábbi elrendezésű szöveget írja ki a képernyőre:

2
22
222
2222
 */
$char = "X";
$sorMax = 10;
for($sor=1;$sor<=$sorMax;$sor++){

    for($j=1;$j<=$sor;$j++){
        echo $char;//karakter kiírása (sornyi)
    }
    echo '<br>';//sortörés kiírása a karakterek után
}
//vége
//visszafelé
for($sor=$sorMax;$sor>=1;$sor--){
    for($j=1;$j<=$sor;$j++){
        echo $char;//karakter kiírása (sornyi)
    }
    echo '<br>';//sortörés kiírása a karakterek után
}
//visszafelé 2
for($sor=$sorMax;$sor>=1;$sor--){
    echo str_repeat($char,$sor);
    echo '<br>';//sortörés kiírása a karakterek után
}


//21. Kérjünk be két természetes számot (M,N), majd rajzoljunk ki a képernyőre egy MxN méretű téglalapot csillag (*) jelekből úgy, hogy a téglalap belseje üres legyen.
/*A   B
 Például M=8 és N=4-re:
                         ********
                         *      *
                         *      *
                         ********
 */
$m = 13;
$n = 6;
$char="*";
$filler="&nbsp; ";
for($sor=1;$sor<=$n;$sor++){
    if($sor == 1 OR $sor == $n){
        echo str_repeat($char,$m);
    }else{
        echo $char.str_repeat($filler,$m-2).$char;
    }
    echo "<br>";
}


//6)
$sor=10;
$oszlop = 5;
//külső ciklus legyen $i ($sor) -> <tr>
// belső ciklus legyen  $j ($oszlop) -> th vagy td


/*
6) table[border=1]>(tr>th{$}*5)+(tr*10>td{.}*5)
7) table>tr*10>th{$}+td{.}*4
*/
//beágyazott ciklussal

$oszlop = 5;
$sor=10;
echo '<table border="1">';
//külső ciklus a soroknak
for($i=0;$i<=$sor;$i++){
    echo '<tr>';
    //belső ciklus a celláknak (oszlop)
    for($j=1;$j<=$oszlop;$j++){

        if($i==0){//első sor (nulladik)
            echo '<th>cella</th>';
        }else{
            echo "<td>$i</td>";
        }
    }
    echo '</tr>';
}

echo '</table>';

//kiemelt 1. oszlop
echo '<table border="1">';
//külső ciklus a soroknak
for($i=1;$i<=$sor;$i++){
    echo '<tr>';
    //belső ciklus a celláknak (oszlop)
    for($j=0;$j<=$oszlop;$j++){

        if($j==0){//első oszlop (nulladik)
            echo '<th>cella</th>';
        }else{
            echo "<td>$j</td>";
        }
    }
    echo '</tr>';
}

echo '</table>';

//23.Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, majd a tömb elemeit átmásolja egy másik tömbbe úgy, hogy az elemek fordított sorrendben helyezkedjenek el benne.
$tomb = [
    'first' => 'első',12, true, 'alma', 'helo', 23/17, 213, 312, 1123, 'utolsó'
];
echo '<pre>'.var_export($tomb,true).'</pre>';
$newArray = [];
/*Ezt így PHP ban nem szerencsés az asszociatív indexek miatt, van rá eljárás ˇˇˇˇ
 * $elements = count($tomb);
foreach($tomb as $k => $v){
    $newArray[$elements-(1+$k)] = $v;
}
echo '<pre>'.var_export($newArray,true).'</pre>';*/

$newArray = array_reverse($tomb);
echo '<pre>'.var_export($newArray,true).'</pre>';